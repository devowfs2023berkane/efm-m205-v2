# Examen Regional de Fin de Module
_Developpement Digital Option Web Full Stack 2eme annee_ `M205` _2023_ v2

## Laravel: Jetstream with Livewire

## Etapes d'installation

### Pour Laravel

```bash
composer i 
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

```bash
php artisan storage:link
```

```bash
php artisan migrate
```
 
- Pour "fake data"

```bash
php artisan migrate:fresh --seed
```

```bash
php artisan serve 
```

### Pour Livewire

```bash
npm i 
```

```bash
npm run dev 
```

