<?php

use App\Http\Controllers\AuteurController;
use App\Http\Controllers\LivreController;
use App\Models\Auteur;
use App\Models\Livre;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::view('/', 'welcome')->name('home');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        $auteurs = Auteur::all();
        $livres = Livre::all();
        return view('dashboard', compact('auteurs', 'livres'));
    })->name('dashboard');

    Route::get('/auteurs', [AuteurController::class, 'index'])->name('auteurs.index');
    Route::get('/insererAuteur', [AuteurController::class, 'create'])->name('auteurs.create');
    Route::post('/storeAuteur', [AuteurController::class, 'store'])->name('auteurs.store');
    Route::get('/auteurs/{auteur}', [AuteurController::class, 'show'])->name('auteurs.show');
    Route::get('/auteurs/{auteur}/edit', [AuteurController::class, 'edit'])->name('auteurs.edit');
    Route::put('/auteurs/{auteur}', [AuteurController::class, 'update'])->name('auteurs.update');
    Route::delete('/auteurs/{auteur}', [AuteurController::class, 'destroy'])->name('auteurs.destroy');

    Route::get('/livres', [LivreController::class, 'index'])->name('livres.index');
    Route::get('/insererLivre', [LivreController::class, 'create'])->name('livres.create');
    Route::post('/storeLivre', [LivreController::class, 'store'])->name('livres.store');
    Route::get('/livres/{livre}', [LivreController::class, 'show'])->name('livres.show');
    Route::get('/livres/{livre}/edit', [LivreController::class, 'edit'])->name('livres.edit');
    Route::put('/livres/{livre}', [LivreController::class, 'update'])->name('livres.update');
    Route::delete('/livres/{livre}', [LivreController::class, 'destroy'])->name('livres.destroy');
});

