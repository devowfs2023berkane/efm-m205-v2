<x-guest-layout>
    {{--    @livewire('navigation-menu')--}}
    {{--@livewire('hello-world')--}}
    {{--@livewire('search-users')--}}
    <nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
        <!-- Primary Navigation Menu -->
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div class="flex justify-between h-16">
                <div class="flex">
                    <!-- Logo -->
                    <div class="shrink-0 flex items-center">
                        <a href="{{ route('home') }}">
                            GestionBibliotheque
                        </a>
                    </div>

                    <!-- Navigation Links -->
                    @auth()
                    <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                        <x-nav-link href="{{ route('dashboard') }}" :active="request()->routeIs('dashboard')">
                            {{ __('Dashboard') }}
                        </x-nav-link>
                        <x-nav-link href="{{ route('auteurs.index') }}" :active="request()->routeIs('auteurs.index')">
                            {{ __('Liste Auteurs') }}
                        </x-nav-link>
                        <x-nav-link href="{{ route('auteurs.create') }}" :active="request()->routeIs('auteurs.create')">
                            {{ __('Ajouter auteur') }}
                        </x-nav-link>
                    </div>
                    @endauth
                </div>
            </div>
        </div>
    </nav>
    <div class="flex justify-center align-middle">
        <h1>Welcome</h1>
    </div>

</x-guest-layout>
