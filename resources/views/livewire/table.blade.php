<div>
    <table class="table bordered striped">
        <thead>
        <tr>
            <th>Numero Auteur</th>
            <th>Nom Auteur</th>
            <th>Adresse Auteur</th>
            <th>Actions</th>
            {{ $header }}
        </tr>
        </thead>
        <tbody>
        @foreach($auteurs as $auteur)
            <tr>
                <td>{{ $auteur->NumAuteur }}</td>
                <td>{{ $auteur->NomAuteur }}</td>
                <td>{{ $auteur->AdresseAuteur }}</td>
                <td>
                    <a href="{{ route('auteurs.show', $auteur->NumAuteur) }}" class="btn btn-info">Afficher les
                        livres</a>
                    <a href="{{ route('auteurs.edit',  $auteur->NumAuteur) }}" class="btn btn-warning">Modifier</a>
                    <form action="{{ route('auteurs.destroy',  $auteur->NumAuteur) }}" method="post"
                          style="display: inline-block">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Supprimer</button>
                    </form>
                </td>
            </tr>
        @endforeach

        {{--        {{ $auteurs->links()}}--}}

        {{ $slot }}
        </tbody>
    </table>
</div>
