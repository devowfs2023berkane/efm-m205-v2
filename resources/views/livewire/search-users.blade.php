<div>
    <input wire:model="search" type="text" placeholder="Search users..."/>
    <ul>
        @forelse($users as $user)
            <li>{{ $user->name }}</li>
        @empty
            <li>No users found</li>
        @endforelse
    </ul>
</div>
