<x-app-layout>
    <h1 class="text-center text-3xl py-5 ">Les Livres d'Auteur <span class="bg-amber-600 p-3"> {{$auteur->NomAuteur}}</span>  </h1>

    <div class="flex justify-center content-center">
        {{--        auteur livres --}}
        <div class="flex justify-center content-center">
            <table class="table-auto">
                <thead>
                <tr>
                    <th>Titre</th>
                    <th>Numero Editeur</th>
                    <th>Annee D'edution</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($auteur->livres as $livre)
                    <tr>
                        <td>{{$livre->TitreLivre}}</td>
                        <td>{{$livre->NumEditeur}}</td>
                        <td>{{$livre->AnneeEdition}}</td>
                        <td>
                            <div class="flex justify-center content-center">
                                <a href="{{route('livres.show', $livre->NumLivre)}}"
                                   class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
                                    Details
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</x-app-layout>
