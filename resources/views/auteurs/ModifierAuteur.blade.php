<x-app-layout>
    <h1 class="text-center text-3xl py-5 ">Modifier Auteur</h1>

    <div class="flex justify-center content-center">
        <form action="{{route('auteurs.update', $auteur->NumAuteur)}}" method="POST">
            @csrf
            @method('PUT')
            <x-input name="NomAuteur" placeholder="Nom" class="p-2 mb-3"
                     value="{{ $auteur->NomAuteur }}"/>
            <br>
            <textarea name="AdresseAuteur"
                      class="border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm p-2 mb-3"
                      id="" cols="30" rows="10" placeholder="Addresse d'auteur">{{  $auteur->AdresseAuteur }}</textarea>
            <br>
            <x-button class="bg-blue-500">Modifier</x-button>
        </form>
    </div>

</x-app-layout>
