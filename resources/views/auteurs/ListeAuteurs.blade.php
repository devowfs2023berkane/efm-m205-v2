<x-app-layout>
    <h1 class="text-center text-3xl py-5 ">Liste des auteurs</h1>
    @if(session()->has('success'))
        <div class="flex bg-green-100 rounded-lg p-4 mb-4 text-sm text-green-700" role="alert">
            <svg class="w-5 h-5 inline mr-3" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                      d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                      clip-rule="evenodd"></path>
            </svg>
            <div>
                <span class="font-medium">{{ session('success') }}</span>
            </div>
        </div>
    @endif
    {{--    @livrewire('table')--}}
    <div>
        <table class="table mx-auto">
            <thead>
            <tr class="bg-yellow-900 text-gray-50">
                <th>Numero Auteur</th>
                <th>Nom Auteur</th>
                <th>Adresse Auteur</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($auteurs as $auteur)
                <tr class="hover:bg-gray-50 border-3 border-bottom border-gray-500 p-2">
                    <td>{{ $auteur->NumAuteur }}</td>
                    <td>{{ $auteur->NomAuteur }}</td>
                    <td>{{ $auteur->AdresseAuteur }}</td>
                    <td>
                        <x-button class="bg-orange-300 hover:bg-orange-500 ">
                            <a href="{{ route('auteurs.show', $auteur->NumAuteur) }}">Afficher les livres</a>
                        </x-button>
                        <x-button class="bg-green-600 hover:bg-green-800">
                            <a href="{{ route('auteurs.edit',  $auteur->NumAuteur) }}">Modifier</a>
                        </x-button>
                        <form action="{{ route('auteurs.destroy',  $auteur->NumAuteur) }}" method="post"
                              onsubmit="return confirm('Voulez vous vraiment supprimer cet auteur?')"
                              style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <x-danger-button type="submit">Supprimer</x-danger-button>
                        </form>
                    </td>
                </tr>
            @endforeach
            {{--            {{ $auteurs->links()}}--}}
            </tbody>
        </table>
    </div>

</x-app-layout>
