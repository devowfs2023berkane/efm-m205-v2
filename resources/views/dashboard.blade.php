<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12 p">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-20 ">
                <p class="text-center">
                    Le nombre totale des auteurs est : {{ $auteurs->count() }} <br>
                Le nombre totale des livres est : {{ $livres->count() }}
                </p>
                <br>
                {{--                le nombre des livres par l'auteurs --}}
                <div class="flex justify-center content-center">
                    <table class="table-auto">
                        <thead class="bg-blue-900 text-gray-50">
                        <tr>
                            <th>Nom Auteur</th>
                            <th>Nombre des livres</th>
                        </tr>
                        </thead>
                        <tbody class="bg-blue-200">
                        @foreach($auteurs as $auteur)
                            <tr>
                                <td>{{$auteur->NomAuteur}}</td>
                                <td class="text-center">{{$auteur->livres->count()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
