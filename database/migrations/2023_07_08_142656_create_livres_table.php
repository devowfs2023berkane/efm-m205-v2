<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('livres', function (Blueprint $table) {
            $table->id('NumLivre');
            $table->string('TitreLivre');
            $table->unsignedBigInteger('AuteurLivre');
            $table->string('NumEditeur');
            $table->string('AnneeEdition');
            $table->foreign('AuteurLivre')->references('NumAuteur')->on('auteurs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('livres');
    }
};
