<?php

namespace Database\Factories;

use App\Models\Auteur;
use App\Models\Livre;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Livre>
 */
class LivreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'TitreLivre' => $this->faker->name,
            'AuteurLivre' => Auteur::all()->random()->NumAuteur,
            'Numediteur' => $this->faker->numberBetween(12021, 10101221),
            'AnneeEdition' => $this->faker->date(),
        ];
    }
}
