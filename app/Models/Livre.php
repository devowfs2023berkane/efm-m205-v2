<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Livre extends Model
{
    use HasFactory;

    protected $primaryKey = 'NumLivre';

    protected $fillable = [
        'TitreLivre',
        'NumEditeur',
        'AnneeEdition',
        'AuteurLivre',
    ];

    public function auteur()
    {
        return $this->belongsTo(Auteur::class);
    }
}
