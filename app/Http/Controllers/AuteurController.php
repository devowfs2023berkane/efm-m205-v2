<?php

namespace App\Http\Controllers;

use App\Models\Auteur;
use Illuminate\Http\Request;

class AuteurController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('auteurs.ListeAuteurs', [
            'auteurs' => Auteur::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('auteurs.InsererAuteur');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'NomAuteur' => 'required|max:200',
            'AdresseAuteur' => 'required|max:200',
        ]);
        Auteur::create($fields);
        return redirect()->route('auteurs.index')->with('success', 'Auteur ete bien ajouté');
    }

    /**
     * Display the specified resource.
     */
    public function show(Auteur $auteur)
    {
        return view('auteurs.Auteur', [
            'auteur' => $auteur,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Auteur $auteur)
    {
        return view('auteurs.ModifierAuteur', [
            'auteur' => $auteur,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Auteur $auteur)
    {
         $fields = $request->validate([
                'NomAuteur' => 'required|max:200',
                'AdresseAuteur' => 'required|max:200',
          ]);
          $auteur->update($fields);
          return redirect()->route('auteurs.index')->with('success', 'Auteur a ete bien modifié');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Auteur $auteur)
    {
//        dd($auteur);
        $auteur->delete();
        return redirect()->route('auteurs.index')->with('success', 'Auteur supprimé');
    }
}
